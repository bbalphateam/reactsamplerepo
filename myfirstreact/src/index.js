import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import './App.css';
import logo from './logo.svg';
//import App from './App';
import * as serviceWorker from './serviceWorker';

//ReactDOM.render(
//  <React.StrictMode>
//    <App />
//  </React.StrictMode>,
//  document.getElementById('root')
//);

//class Test extends Component {
//    render() {
//        return <h1>React Version: {React.version} </h1>
//    }
//}

//class App extends Component {
//    render() {
//        return (
//            <div className="App">
//                <header className="App-header">
//                    <img src={logo} className="App-logo" alt="logo" />
//                    <a href="https://www.google.com" target="_blank">Google</a>
//                </header>
//            </div>

//            );
//    }
//}
//export default App;
const myHtml = (
    <table>
        <tr>
            <th>
                Name:
            </th>
        </tr>
        <tr>
            <th>
                Age:
            </th>
        </tr>
        <tr>
            <th>
                Sex:
            </th>
        </tr>
    </table>
)
ReactDOM.render(myHtml, document.getElementById('RenderHtml'));
// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
